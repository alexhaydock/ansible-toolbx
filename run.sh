#!/bin/sh
set -e

# Install Ansible
if which ansible-playbook >/dev/null 2>/dev/null; then echo "Ansible is installed."; else sudo dnf install -y ansible ; exit 5; fi

# Run Ansible playbook
ansible-playbook playbook.yml --become-password-file password.txt
