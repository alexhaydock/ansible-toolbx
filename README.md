# ansible-toolbx

Run me inside a Fedora [Toolbx](https://containertoolbx.org/) container.

## Usage:
```sh
sudo dnf install toolbox
toolbox enter
./run.sh
```
